from django.shortcuts import render
from django.views.generic import ListView, DetailView
from manager.models import Job
from django.contrib.auth.decorators import login_required


# Create your views here.
class JobListView(ListView):
    model = Job
    context_object_name = 'jobs'

job_list_view = login_required(JobListView.as_view())


class JobDetailView(DetailView):
    model = Job
    context_object_name = 'details'
