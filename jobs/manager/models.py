from django.db import models


class Customer(models.Model):
    name = models.CharField(max_length=256)
    address = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    state = models.CharField(max_length=256)
    zip_code = models.IntegerField()
    balance = models.IntegerField()

    def __str__(self):
        return self.name


class Job(models.Model):
    job_number = models.AutoField(primary_key=True)
    title = models.CharField(max_length=256)
    desc = models.TextField()
    completed = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=19, decimal_places=2)
    customer = models.ForeignKey(Customer)
    date_entered = models.DateTimeField(auto_now_add=True)
    date_due = models.DateTimeField()

    def __str__(self):
        return self.title


class Note(models.Model):
    DEPARTMENTS = (
            ('ACCT', 'Accounting'),
            ('CSR', 'Customer Service'),
            ('PRE', 'Prepress'),
            ('PRS', 'Press'),
            ('BND', 'Bindery'),
            ('SHP', 'Shipping')
            )
    job = models.ForeignKey(Job)
    department = models.CharField(max_length=256, choices=DEPARTMENTS)
    desc = models.TextField()

    def __str__(self):
        return self.department
