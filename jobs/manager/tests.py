from django.test import TestCase
from .models import Job, Customer, Note
from datetime import date


class RootPageTest(TestCase):
    def test_root_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


class CustomerCreation(TestCase):
    def setUp(self):
        Customer.objects.create(name='test customer', address='808 Blaine',
                                city='Boyden', state='IA', zip_code=51234,
                                balance=0)

    def test_customer_created(self):
        customer1 = Customer.objects.get(name='test customer')
        self.assertEqual('test customer', customer1.name, customer1.__str__())


class JobCreation(TestCase):
    def setUp(self):
        Customer.objects.create(name='test customer', address='808 Blaine',
                                city='Boyden', state='IA', zip_code=51234,
                                balance=0)
        customer1 = Customer.objects.get(name='test customer')
        Job.objects.create(title='test title', desc='test description',
                           price=100, customer=customer1, date_due=date.today())

    def test_job_was_created(self):
        job = Job.objects.get(title='test title')
        self.assertEqual('test title', job.title, job.__str__())


class NoteCreation(TestCase):
    def setUp(self):
        Customer.objects.create(name='test customer', address='808 Blaine',
                                city='Boyden', state='IA', zip_code=51234,
                                balance=0)
        customer1 = Customer.objects.get(name='test customer')
        Job.objects.create(title='test title', desc='test description',
                           price=100, customer=customer1, date_due=date.today())
        job1 = Job.objects.get(title='test title')
        Note.objects.create(department='Accounting', desc='test note', job=job1)

    def test_note_creation(self):
        note1 = Note.objects.get(desc='test note')
        self.assertEqual('test note', note1.desc)
        self.assertEqual('Accounting', note1.__str__())


class ModifyCustomerBalance(TestCase):
    #TODO: Make sure adding a job subtracts from customer's balance

