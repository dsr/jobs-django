from django.contrib import admin
from .models import Job
from .models import Note
from .models import Customer


class NoteInline(admin.TabularInline):
    model = Note
    extra = 1


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    inlines = [NoteInline,]
    list_display = ('job_number', 'title', 'desc', 'customer')


admin.site.register(Customer)