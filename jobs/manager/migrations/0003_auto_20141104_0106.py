# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0002_auto_20141030_0113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='job',
            name='id',
        ),
        migrations.AddField(
            model_name='job',
            name='job_number',
            field=models.AutoField(default=0, serialize=False, primary_key=True),
            preserve_default=True,
        ),
    ]
