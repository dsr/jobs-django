# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=256)),
                ('desc', models.TextField()),
                ('completed', models.BooleanField(default=False)),
                ('price', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('department', models.CharField(choices=[('ACCT', 'Accounting'), ('CSR', 'Customer Service'), ('PRE', 'Prepress'), ('PRS', 'Press'), ('BND', 'Bindery'), ('SHP', 'Shipping')], max_length=256)),
                ('desc', models.TextField()),
                ('job', models.ForeignKey(to='manager.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
