# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0003_auto_20141104_0106'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=256)),
                ('address', models.CharField(max_length=256)),
                ('city', models.CharField(max_length=256)),
                ('state', models.CharField(max_length=256)),
                ('zip_code', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='job',
            name='customer',
            field=models.ForeignKey(default=1, to='manager.Customer'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='job',
            name='date_due',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2014, 11, 4, 1, 21, 13, 635338, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='job',
            name='date_entered',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2014, 11, 4, 1, 21, 21, 371213, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='job',
            name='job_number',
            field=models.AutoField(serialize=False, primary_key=True),
            preserve_default=True,
        ),
    ]
