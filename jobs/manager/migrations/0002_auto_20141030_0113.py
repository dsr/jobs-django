# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='job',
            name='price',
            field=models.DecimalField(max_digits=19, decimal_places=2),
            preserve_default=True,
        ),
    ]
