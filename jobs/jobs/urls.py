from django.conf.urls import patterns, include, url
from django.contrib import admin
from manager.views import JobListView, job_list_view
from manager.views import JobDetailView


urlpatterns = patterns('',
    # Examples:
    url(r'^$', job_list_view, name='home'),
    url(r'^job/(?P<pk>\d+)', JobDetailView.as_view(), name='detail'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='auth_logout'),
    url(r'^admin/', include(admin.site.urls)),
)
